/*
 * Copyright (C) 2012 Atlassian
 *
 * Author:
 *	Ian Monroe <ian.monroe@atlassian.com>
 *
 * Source:
 *	http://code.google.com/p/qxmpp
 *
 * This file is a part of QXmpp library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef MUCDIALOG_H
#define MUCDIALOG_H

#include <QDialog>
#include <QStringList>
#include <QWeakPointer>

#include "QXmppMucManager.h"

namespace Ui {
class mucDialog;
}

class QStringListModel;

class mucDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit mucDialog(QXmppMucRoom* room, QWidget *parent);
    ~mucDialog();

private slots:
    void onParticipantsChanged();
    void onError(QXmppStanza::Error);
    void onMessageReceived(const QXmppMessage &message);
    void onSubjectChanged(const QString& subject);

    void sendMessage();

private:
    void addMessage(const QString& from, const QString& body);

    Ui::mucDialog *m_ui;
    QStringListModel* m_participantModel;
    QWeakPointer<QXmppMucRoom> m_room;

};

#endif // MUCDIALOG_H
