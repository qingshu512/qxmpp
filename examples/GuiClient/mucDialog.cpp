/*
 * Copyright (C) 2012 Atlassian
 *
 * Author:
 *	Ian Monroe <ian.monroe@atlassian.com>
 *
 * Source:
 *	http://code.google.com/p/qxmpp
 *
 * This file is a part of QXmpp library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#include "mucDialog.h"
#include "ui_mucDialog.h"
#include "chatGraphicsScene.h"
#include "chatGraphicsView.h"

#include "QXmppMessage.h"

#include <QDebug>
#include <QLabel>
#include <QStringListModel>
#include <QLineEdit>
#include <QTextEdit>

mucDialog::mucDialog(QXmppMucRoom* room, QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::mucDialog)
  , m_participantModel(new QStringListModel)
  , m_room(room)
{
    qDebug() << "we got" << room << room->subject() << room->jid() << m_room.data();
    m_ui->setupUi(this);

    onSubjectChanged(room->subject());
    onParticipantsChanged();
    m_ui->listView->setModel(m_participantModel);

    connect( room, SIGNAL(participantAdded(const QString&)), this, SLOT( onParticipantsChanged() ) );
    connect( room, SIGNAL(participantChanged(const QString&)), this, SLOT( onParticipantsChanged()) );
    connect( room, SIGNAL(participantRemoved(const QString&)), this, SLOT( onParticipantsChanged() ) );
    connect( room, SIGNAL(messageReceived(QXmppMessage)), this, SLOT(onMessageReceived(QXmppMessage) ) );
    connect( room, SIGNAL(error(QXmppStanza::Error)), this, SLOT(onError(QXmppStanza::Error)) );
    connect( room, SIGNAL(subjectChanged(QString)), this, SLOT(onSubjectChanged(QString)));
    connect( m_ui->sendButton, SIGNAL(clicked()), this, SLOT(sendMessage()));
    show();
}

mucDialog::~mucDialog()
{
    m_room.data()->leave("See ya folks");
    delete m_ui;
    delete m_room.data();
}

void mucDialog::onParticipantsChanged()
{
    QStringList parts = m_room.data()->participants();
    parts.sort();
    m_participantModel->setStringList(parts);
}

void mucDialog::onError(QXmppStanza::Error error)
{
    qDebug() << "Error" << error.type() << error.condition() << error.text();
    m_ui->chatText->insertHtml(QString("<b><font color='red'>Error: %1</font></b><br>").arg(error.text()));
}


void mucDialog::onMessageReceived(const QXmppMessage &message)
{
    qDebug() << message.type() << message.state() << message.from() << message.body();
    addMessage(message.from(), message.body());
}

void mucDialog::onSubjectChanged(const QString &subject)
{
    m_ui->subject->setText(subject);
}

void mucDialog::sendMessage()
{
    QString body = m_ui->lineEdit->text();
    m_room.data()->sendMessage(body);
    m_ui->lineEdit->clear();
}

void mucDialog::addMessage(const QString &from, const QString &body)
{
    m_ui->chatText->insertHtml(QString("<b>&lt;%1&gt;</b> %2<br>").arg(from, body));
}
